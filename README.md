# List of professional learning/certificates I have been doing in recent years #

* [React certified](https://drive.google.com/file/d/1ZZVKM-opeNFK_O_tE3QY1M8K9jhjzHpx/view?usp=drive_link), May 2024

* [Progressive Web Apps](https://drive.google.com/file/d/1C2xW9IMbQrKBgTlf8Z-ugDd0eqPoq6Wr/view?usp=drive_link), Aug 2023

* [Conflict Management](https://drive.google.com/file/d/1kuew-11BOyE39epCVLxiXUsGDhhDQHtm/view?usp=drive_link), Jun 2023

* [The Nuts and Bolts of OAuth 2.0](https://drive.google.com/file/d/1UZ7CIok_N9HmgJF_t6nJ1al4OtC6KB0L/view?usp=drive_link), May 2023

* [Practical Leadership Skills](https://drive.google.com/file/d/1817AiMMZ1m2vvjwwp0xZ-BpzK6UJ4Orq/view?usp=drive_link), May 2023

* [Negotiation Skills](https://drive.google.com/file/d/1kRHSsm2n9RrBHhaijFnsUfL2RMpd4r6y/view?usp=drive_link), Dec 2022

* [Assertive Communication Skills](https://drive.google.com/file/d/1DU9cia3hEJjxxvcHPaJ77r-qwlEvadmq/view?usp=drive_link), Oct 2022

* [Feature Engineering](https://coursera.org/share/d419d5d0526985d3bb23417409e8464b), May 2022

* [Google Cloud Platform Fundamentals: Core Infrastructure](https://coursera.org/share/949c1662ab959ae8ee5adbce42802932), Aug 2020

* [Reliable Google Cloud Infrastructure: Design and Process](https://coursera.org/share/c8f19379e6540d91b8b6162b8678958a), Aug 2020

* [Neural Networks and Deep Learning](https://coursera.org/share/19cc19b5160a73c834c4e0229528da4b), Jan 2020 

* [Leveraging Unstructured Data with Cloud Dataproc on Google Cloud Platform](https://coursera.org/share/780d7b6788313f71893ab6e8b1eedf5e), Dec 2019

* [Google Cloud Building Resilient Streaming Systems on Google Cloud Platform](https://coursera.org/share/9f15e468ab060c6b4255715de5fdc66f), Dec 2019

* [Serverless Data Analysis with Google BigQuery and Cloud Dataflow](https://coursera.org/share/12e653b6226a36643d6798b27120822e), Nov 2019

* [Serverless Machine Learning with Tensorflow on Google Cloud Platform](https://coursera.org/share/a603a514571d5107654b4dbd49fd0519), Nov 2019

* [Google Cloud Platform Big Data and Machine Learning Fundamentals](https://coursera.org/share/050c9ca9b8991fdf2565eb81b540732f), Nov 2019

* [Elasticsearch Engineer 2](https://www.elastic.co/training/elasticsearch-engineer-2), in-class training by Elastic co, Aug 2019


